import qrcode
from qrcode import constants

def qrCode():
    qr = qrcode.QRCode(version=1,error_correction=constants.ERROR_CORRECT_M, box_size=15, border=4)
    first_input = input('Enter your URL for QR-code: ')
    qr.add_data(first_input)
    qr.make(fit=True)
    first_config= input('Enter fill color: ')
    second_config= input('Enter back ground color: ')
    code = qr.make_image(fill_color =first_config, back_color=second_config)
    code.save('First QR.png')

if __name__ =='__main__':
    qrCode()