# QrCodeScript


QrCode creating App


## Installation

You need install python 3.9:

```
pip install python
```

## Important import
```
import qrcode
from qrcode import constants

```

## Getting started

App strat:

```
python main.py
```

## Technologies used

- Python
- JetBrains
- Qrcode


## Description

After start of program you need input: ur URL, fill color and back ground color.
Program will generate QR for you.
App was create just for fun :)


## Author

This app was done by Dmitry Tsunaev.

- [linkedin](http://linkedin.com/in/dmitry-tsunaev-530006aa)

